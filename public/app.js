if (window.File && window.FileReader && window.FileList && window.Blob) {
  // Great success! All the File APIs are supported.
} else {
  alert('The File APIs are not fully supported in this browser.');
}

var files =document.getElementById('files');
var dropZone = document.getElementById('dropZone');
var list = document.getElementById('list');
var gallery = document.getElementById('gallery');

function handleFileSelect(e) {
  e.stopPropagation();
  e.preventDefault();
  files.files = e.target.files || e.dataTransfer.files;
  var filesList = files.files;
  var output = [];
  for (var i = 0, f; f = filesList[i]; i++) {
    output.push(
      '<li><strong>', escape(f.name), '</strong> (', f.type || 'n/a', ') - ',
      f.size, ' bytes, last modified: ',
      f.lastModifiedDate.toLocaleDateString(), '</li>'
    );

    if (!f.type.match('image.*')) continue;
    var reader = new FileReader();
    reader.onload = (function(theFile) {
      return function(e) {
        sendFile(e.target.result, theFile.name);
        var span = document.createElement('span');
        span.innerHTML = [
          '<img class="thumb" src="', e.target.result,
          '" title="', escape(theFile.name), '" width=200 />'
        ].join('');
        list.insertBefore(span, null);
      };
    })(f);
    
    reader.readAsDataURL(f)
  }
  list.innerHTML = '<ul>' + output.join('') + '</ul>';
}

function handleDragOver(e) {
  e.stopPropagation();
  e.preventDefault();
  e.dataTransfer.dropEffect = 'copy';
}

function sendFile(dataUrl, name) {
  var form = new FormData();
  form.append('name', name);
  form.append('img', dataUrl);

  var xhr = new XMLHttpRequest();
  xhr.onload = function() {
      console.log('Image sent');
      showGallery();
  };
  xhr.open('post', '/img/add', true);
  xhr.send(form);
}

function showGallery() {
  var xhr = new XMLHttpRequest();
  xhr.onload = function(data) {
    var images = JSON.parse(data.currentTarget.response);
    gallery.innerHTML = images.map(function (img) {
      return '<img src="uploads/' + img + '" name="' + img + '" width=200/>';
    }).join('');
    [].slice.call(document.querySelectorAll('#gallery img')).map(function (img) {
      img.addEventListener('click', deleteImage, false);
    });
  };
  xhr.open('post', '/img/getList', true);
  xhr.send();
}

function deleteImage(e) {
  var xhr = new XMLHttpRequest();
  xhr.onload = function() {
    showGallery();
  };
  xhr.open('post', '/img/del', true);
  xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
  xhr.send(JSON.stringify({img: e.target.name}));
}

showGallery();

files.addEventListener('change', handleFileSelect, false);
dropZone.addEventListener('dragover', handleDragOver, false);
dropZone.addEventListener('drop', handleFileSelect, false);
dropZone.addEventListener('click', function(e) {files.click();}, false);

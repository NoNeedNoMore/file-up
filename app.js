'use strict';

const port = process.env.PORT || 1337;
const express = require('express');
const app = express();
const compression = require('compression');
const bodyParser = require('body-parser');
const http = require('http');
const server = http.createServer(app);
const multer  = require('multer');
let uploadDir = __dirname + '/public/uploads/';
const upload = multer({limits: {fieldSize: 5242880}, dest: uploadDir });
const fs = require('fs');

app
  .use ( compression() )
  .use ( bodyParser.json() )
  .use ( express.static(__dirname + '/public'))
  .get ( '/', (req, res) => res.redirect('index.html'))
  .get ( '*.html', (req, res) => res.redirect('index.html'))
  .post( '/img/add', upload.array('images', 13), (req, res) => {
    const name = req.body.name;
    const base64Data = req.body.img.replace(/^data:image\/\w+;base64,/, '');
    fs.writeFile(uploadDir + name, base64Data, {encoding: 'base64'}, err => {
      if(err) return res.status(200).send('error');
      res.status(200).send('Ok');
    });
  })
  .post( '/img/del', (req, res) => {
    fs.unlink(uploadDir + req.body.img, err => {
      if(err) return res.status(200).send('error');
      res.status(200).send('Ok');
    });
  }).post( '/img/getList', (req, res) => {
    fs.readdir(uploadDir, (err, result) => {
      if(err) return res.status(200).send('error');
      res.status(200).json(result);
    });
  });

server.listen(port, () => {console.log(`${new Date()} Listening at ${port}`);});
